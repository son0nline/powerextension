var l = document.links;
for (var i = 0; i < l.length; i++) {
  if (l[i].href.match(/.*redirect.index.php.link*/g)) {
    var result = l[i].href.match(/link=.*/g);
    if (result != null) {
      var str = unescape(String(result));
      str = str.replace(/link=/g, "");
      l[i].setAttribute('href', str);
    }
  }
}

var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
link.type = 'image/x-icon';
link.rel = 'shortcut icon';
link.href = 'https://www.stackoverflow.com/favicon.ico';
document.getElementsByTagName('head')[0].appendChild(link);
document.getElementsByTagName("title")[0].innerText = "Stack Overflow - Where Developers Learn, Share, & Build Careers";