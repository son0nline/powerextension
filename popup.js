function doSomething(e) {
  // chrome.tabs.executeScript(null, {
  //   code: "console.log('"+ JSON.stringify(e.target.id) + "')"
  // });

  // chrome.tabs.getCurrent(function (tab) {
  //   chrome.tabs.executeScript(null, {
  //     code: 'console.log(' + JSON.stringify(tab) + ');'
  //   });
  // });

  if (e.target.id == "prev") {
    chrome.tabs.executeScript(null, {
      file: "aplayerPrevious.js"
    });
  } else if (e.target.id == "play") {
    chrome.tabs.executeScript(null, {
      file: "content.js"
    });
  } else if (e.target.id == "next") {
    chrome.tabs.executeScript(null, {
      file: "aplayerNext.js"
    });
  }
  window.close();
}

document.addEventListener('DOMContentLoaded', function () {
  var divs = document.getElementsByClassName('ctrl');
  for (var i = 0; i < divs.length; i++) {
    divs[i].addEventListener('click', doSomething);
  }

  var input = document.getElementById("idx");
  input.addEventListener('keyup', function (event) {
    if (event.keyCode === 13) {
      if(input.value.match(/^\d+$/g))
      {
        chrome.tabs.executeScript(null, {
          code: '$(".aplayer-list-index:contains(' + "'" + input.value + "'" + ')")[0].parentElement.click();'
        });
      } else if (input.value[0] == "?") {
        var newURL = 'https://encrypted.google.com/#q=' + encodeURIComponent(input.value.substring(1).trim());
        chrome.tabs.update({ url: newURL });
      } else {
        var newURL = 'https://soundcloud.com/search?q=' + encodeURIComponent(input.value);
        chrome.tabs.update({ url: newURL });

        // queryInfo = new Object();
        // queryInfo.active = true;
        // chrome.tabs.query(queryInfo, function(result) {
        //     var activeTab = result[1].id;
        //     updateProperties = new Object();
        //     updateProperties.url = action_url;
        //     chrome.tabs.update(activeTab, updateProperties, function() {
        //           // Anything else you want to do after the tab has been updated.
        //     });
        // });
      }
      window.close();
    }
  });
});